package p1;

import java.io.*;
import java.net.URL;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class PeersServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String endPoint = req.getParameter("endpoint");
		resp.setContentType("text/plain");
	    URL peersurl = new URL("http://step-test-krispop.appspot.com/peers?endpoint=" + endPoint);
	    BufferedReader peersReader = new BufferedReader(new InputStreamReader(peersurl.openStream()));
	    String peersline;
	    while ((peersline = peersReader.readLine()) != null) {
			resp.getWriter().println(peersline);	
	    }
	    peersReader.close();
	}
}