package p1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.http.*;

@SuppressWarnings("serial")
public class ShowServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String message = req.getParameter("message");
		resp.setContentType("text/plain");
		
		List<String> listURL = new ArrayList<String>();
	    URL peersurl = new URL("http://step-test-krispop.appspot.com/peers");
	    BufferedReader peersReader = new BufferedReader(new InputStreamReader(peersurl.openStream()));
	    String peersline;
	    while ((peersline = peersReader.readLine()) != null) {
			listURL.add(peersline);
	    }
	    peersReader.close();
	    
	    Random r = new Random();
	    URL url = new URL(listURL.get(r.nextInt(listURL.size())) + "/convert?message=" + message);
	    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
	    String line;
	    while ((line = reader.readLine()) != null) {
			resp.getWriter().println(line);
	    }
	    reader.close();
	}
}